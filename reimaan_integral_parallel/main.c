#include <stdio.h>
#include <omp.h>

#define NUM_THREADS 8
static long num_steps = 100000;
double step;

void main() {

    double x,pi,sum = 0.0;
    step = 1.0/(double)num_steps;
    int i;

    #pragma omp parallel
    {   double x;
        #pragma omp parallel for reduction(+:sum)
            for (i=0; i<num_steps; i++) {
                x = (i + 0.5) * step;
                sum += 4.0 / (1.0 + x * x);
            }
    }

    pi = sum * step;

    printf("Pi is %lf\n",pi);
}

