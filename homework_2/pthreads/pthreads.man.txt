


Threads Library Functions                           threads(3THR)



NAME
     threads, pthreads, libpthread, libthread - concepts  related
     to POSIX pthreads and Solaris threads and the libpthread and
     libthread libraries

SYNOPSIS
  POSIX
     cc -mt [ flag... ] file...- lpthread [ -lposix4 library... ]

     #include <pthread.h>

  Solaris
     cc - mt [ flag... ] file...[ library... ]

     #include <sched.h>

     #include <thread.h>

DESCRIPTION
     POSIX and Solaris threads each have their own implementation
     of  the  threads library. The  libpthread library is associ-
     ated with POSIX; the libthread library  is  associated  with
     Solaris. Both implementations are interoperable, their func-
     tionality similar, and can be used within the same  applica-
     tion. Only POSIX threads are guaranteed to be fully portable
     to other POSIX-compliant  environments.  POSIX  and  Solaris
     threads  require different source, include files and linking
     libraries. See SYNOPSIS.

  Similarities
     Most of the  functions  in  the  libpthread  and  libthread,
     libraries  have  a  counterpart  in  the other corresponding
     library. POSIX function names, with  the  exception  of  the
     semaphore names, have a "pthread" prefix. Function names for
     similar POSIX and Solaris have similar  endings.  Typically,
     similar POSIX and Solaris functions have the same number and
     use of arguments.

  Differences
     POSIX pthreads and Solaris threads differ in  the  following
     ways:

        o  POSIX threads are more portable.

        o  POSIX  threads  establish  characteristics   for  each
           thread according to configurable attribute objects.

        o  POSIX pthreads implement thread cancellation.

        o  POSIX pthreads enforce scheduling algorithms.

        o  POSIX pthreads allow for clean-up handlers for fork(2)



SunOS 5.8           Last change: 22 Oct 1999                    1






Threads Library Functions                           threads(3THR)



           calls.

        o  Solaris threads can be suspended and continued.

        o  Solaris  threads  implement  an  optimized  mutex  and
           interprocess robust mutex locks.

        o  Solaris threads implement daemon  threads,  for  whose
           demise the process does not wait.

Function Comparison
     The following table compares the POSIX pthreads and  Solaris
     threads functions. When a comparable interface is not avail-
     able either in POSIX pthreads  or Solaris threads, a  hyphen
     (-) appears in the column.

  Functions Related to Creation
           POSIX (libpthread)             Solaris (libthread)
     pthread_create()                 thr_create()
     pthread_attr_init()              -
     pthread_attr_setdetachstate()    -
     pthread_attr_getdetachstate()    -
     pthread_attr_setinheritsched()   -
     pthread_attr_getinheritsched()   -
     pthread_attr_setschedparam()     -
     pthread_attr_getschedparam()     -
     pthread_attr_setschedpolicy()    -
     pthread_attr_getschedpolicy()    -
     pthread_attr_setscope()          -
     pthread_attr_getscope()          -
     pthread_attr_setstackaddr()      -
     pthread_attr_getstackaddr()      -
     pthread_attr_setstacksize()      -
     pthread_attr_getstacksize()      -
     pthread_attr_getguardsize()      -
     pthread_attr_setguardsize()      -
     pthread_attr_destroy()           -
     -                                thr_min_stack()


  Functions Related to Exit
         POSIX (libpthread)            Solaris (libthread)
     pthread_exit()                thr_exit()
     pthread_join()                thr_join()
     pthread_detach()              -


  Functions Related to Thread Specific Data
         POSIX (libpthread)            Solaris (libthread)
     pthread_key_create()          thr_keycreate()
     pthread_setspecific()         thr_setspecific()
     pthread_getspecific()         thr_getspecific()



SunOS 5.8           Last change: 22 Oct 1999                    2






Threads Library Functions                           threads(3THR)



     pthread_key_delete()          -


  Functions Related to Signals
         POSIX (libpthread)            Solaris (libthread)
     pthread_sigmask()             thr_sigsetmask()
     pthread_kill()                thr_kill()


  Functions Related to IDs
         POSIX (libpthread)            Solaris (libthread)
     pthread_self()                thr_self()
     pthread_equal()               -
     -                             thr_main()


  Functions Related to Scheduling
         POSIX (libpthread)            Solaris (libthread)
     -                             thr_yield()
     -                             thr_suspend()
     -                             thr_continue()
     pthread_setconcurrency()      thr_setconcurrency()
     pthread_getconcurrency()      thr_getconcurrency()
     pthread_setschedparam()       thr_setprio()
     pthread_getschedparam()       thr_getprio()


  Functions Related to Cancellation
         POSIX (libpthread)            Solaris (libthread)
     pthread_cancel()              -
     pthread_setcancelstate()      -
     pthread_setcanceltype()       -
     pthread_testcancel()          -
     pthread_cleanup_pop()         -
     pthread_cleanup_push()        -


  Functions Related to Mutexes
               POSIX (libpthread)             Solaris (libthread)
     pthread_mutex_init()                     mutex_init()
     pthread_mutexattr_init()                 -
     pthread_mutexattr_setpshared()           -
     pthread_mutexattr_getpshared()           -
     pthread_mutexattr_setprotocol()          -
     pthread_mutexattr_getprotocol()          -
     pthread_mutexattr_setprioceiling()       -
     pthread_mutexattr_getprioceiling()       -
     pthread_mutexattr_settype()              -
     pthread_mutexattr_gettype()              -
     pthread_mutexattr_destroy()              -
     pthread_mutex_setprioceiling()           -
     pthread_mutex_getprioceiling()           -



SunOS 5.8           Last change: 22 Oct 1999                    3






Threads Library Functions                           threads(3THR)



     pthread_mutex_lock()                     mutex_lock()
     pthread_mutex_trylock()                  mutex_trylock()
     pthread_mutex_unlock()                   mutex_unlock()
     pthread_mutex_destroy()                  mutex_destroy()


  Functions Related to Condition Variables
          POSIX (libpthread)             Solaris (libthread)
     pthread_cond_init()             cond_init()
     pthread_condattr_init()         -
     pthread_condattr_setpshared()   -
     pthread_condattr_getpshared()   -
     pthread_condattr_destroy()      -
     pthread_cond_wait()             cond_wait()
     pthread_cond_timedwait()        cond_timedwait()
     pthread_cond_signal()           cond_signal()
     pthread_cond_broadcast()        cond_broadcast()
     pthread_cond_destroy()          cond_destroy()


  Functions Related to Reader/Writer Locking
           POSIX (libpthread)              Solaris (libthread)
     pthread_rwlock_init()             rwlock_init()
     pthread_rwlock_rdlock()           rw_rdlock()
     pthread_rwlock_tryrdlock()        rw_tryrdlock()
     pthread_rwlock_wrlock()           rw_wrlock()
     pthread_rwlock_trywrlock()        rw_trywrlock()
     pthread_rwlock_unlock()           rw_unlock()
     pthread_rwlock_destroy()          rwlock_destroy()
     pthread_rwlockattr_init()         -
     pthread_rwlockattr_destroy()      -
     pthread_rwlockattr_getpshared()   -
     pthread_rwlockattr_setpshared()   -


  Functions Related to Semaphores
         POSIX (libpthread)            Solaris (libthread)
     sem_init()                    sema_init()
     sem_open()                    -
     sem_close()                   -
     sem_wait()                    sema_wait()
     sem_trywait()                 sema_trywait()
     sem_post()                    sema_post()
     sem_getvalue()                -
     sem_unlink()                  -
     sem_destroy()                 sema_destroy()


  Functions Related to fork() Clean Up
         POSIX (libpthread)            Solaris (libthread)
     pthread_atfork()              -




SunOS 5.8           Last change: 22 Oct 1999                    4






Threads Library Functions                           threads(3THR)



  Functions Related to Limits
         POSIX (libpthread)            Solaris (libthread)
     pthread_once()                -


  Functions Related to Debugging
         POSIX (libpthread)            Solaris (libthread)
     -                             thr_stksegment()


LOCKING
  Synchronization
     POSIX   (libpthread)         Solaris   (libthread)    Multi-
     threaded behavior is asynchronous, and therefore,  optimized
     for concurrent and parallel processing.  As threads,  always
     from  within  the  same process and  sometimes from multiple
     processes, share global data with each other, they  are  not
     guaranteed exclusive
      access to the shared data at any point  in  time.  Securing
     mutually exclusive access to shared data requires  synchron-
     ization among the threads. Both POSIX and Solaris  implement
     four  synchronization  mechanisms:  mutexes, condition vari-
     ables,  reader/writer  locking    (optimized   frequent-read
     occasional-write mutex), and semaphores.

     Synchronizing multiple threads diminishes their concurrency.
     The  coarser  the  grain  of  synchronization,  that is, the
     larger the block of code that is locked, the lesser the con-
     currency.

  MT fork()
     If a POSIX threads  program  calls  fork(2),  it  implicitly
     calls  fork1(2),  which  replicates only the calling thread.
     Should there be any outstanding mutexes throughout the  pro-
     cess,  the  application should call pthread_atfork(3THR), to
     wait for and acquire those mutexes, prior to calling fork().

SCHEDULING
  POSIX
     Scheduling allocation size per thread is greater  than  one.
     POSIX supports the following three scheduling policies:

     SCHED_OTHER
           Timesharing (TS) scheduling policy. It is based on the
           timesharing scheduling class.

     SCHED_FIFO
           First-In-First-Out (FIFO) scheduling  policy.  Threads
           scheduled  to  this  policy,  if  not  pre-empted by a
           higher  priority,  will  proceed   until   completion.
           Threads    whose    contention    scope    is   system
           (PTHREAD_SCOPE_SYSTEM)   are   in    real-time    (RT)



SunOS 5.8           Last change: 22 Oct 1999                    5






Threads Library Functions                           threads(3THR)



           scheduling  class.  The  calling  process  must have a
           effective user ID of 0. SCHED_FIFO for  threads  whose
           contention  scope's process (PTHREAD_SCOPE_PROCESS) is
           based on the TS scheduling class.

     SCHED_RR
           Round-Robin scheduling policy.  Threads  scheduled  to
           this  policy,  if not pre-empted by a higher priority,
           will execute for a time period determined by the  sys-
           tem.   Threads   whose   contention  scope  is  system
           (PTHREAD_SCOPE_SYSTEM) are in real-time (RT)  schedul-
           ing  class  and the calling process must have a effec-
           tive user ID of 0. SCHED_RR for threads whose  conten-
           tion scope is process (PTHREAD_SCOPE_PROCESS) is based
           on the TS scheduling class.

  Solaris
     Only scheduling policy supported is  SCHED_OTHER,  which  is
     timesharing, based on the TS scheduling class.

ALTERNATE IMPLEMENTATION
     The standard threads implementation is a two-level model  in
     which user-level threads are multiplexed over possibly fewer
     lightweight processes, or LWPs.  An LWP is  the  fundamental
     unit  of  execution that is dispatched to a processor by the
     operating system.

     The system provides an alternate threads  implementation,  a
     one-level  model, in which user-level threads are associated
     one-to-one with LWPs.  This implementation is  simpler  than
     the  standard  implementation  and may be beneficial to some
     multithreaded applications.  It provides  exactly  the  same
     interfaces,  both  for POSIX threads and Solaris threads, as
     the standard implementation.

     To link with the alternate implementation, use the following
     runpath (-R) options when linking the program:

  POSIX
     cc -mt ... -lpthread ... -R /usr/lib/lwp     (32-bit)
     cc -mt ... -lpthread ... -R /usr/lib/lwp/64  (64-bit)

  Solaris
     cc -mt ... -R /usr/lib/lwp     (32-bit)
     cc -mt ... -R /usr/lib/lwp/64  (64-bit)

     For multithreaded programs that have been previously  linked
     with the standard threads library, the environment variables
     LD_LIBRARY_PATH and LD_LIBRARY_PATH_64 can be set as follows
     to  bind  the  program  at  runtime to the alternate threads
     library:




SunOS 5.8           Last change: 22 Oct 1999                    6






Threads Library Functions                           threads(3THR)



     LD_LIBRARY_PATH=/usr/lib/lwp
     LD_LIBRARY_PATH_64=/usr/lib/lwp/64

     Note that if an LD_LIBRARY_PATH environment variable  is  in
     effect  for  a  secure process, then only the trusted direc-
     tories specified by this variable will be used  to   augment
     the runtime linker's search rules.

     The runtime linker may also be instructed to use  this  lib-
     thread  by  establishing  an  alternative  object cache; see
     crle(1) with the -a option.

     When using the alternate one-level  threads  implementation,
     be  aware  that  it  may  create more LWPs than the standard
     implementation using unbound threads.  LWPs consume  operat-
     ing system memory in contrast to threads, which consume only
     user-level memory.  Thus a multithreaded application  linked
     against this library that creates thousands of threads would
     create an equal number of LWPs and might run the system  out
     of resources required to support the application.

ERRORS
     In a multi-threaded application, linked with  libpthread  or
     libthread,  EINTR  may  be  returned whenever another thread
     calls  fork(2), which calls  fork1(2) instead.

ATTRIBUTES
     See attributes(5) for descriptions of the  following  attri-
     butes:

     ____________________________________________________________
    |       ATTRIBUTE TYPE        |       ATTRIBUTE VALUE       |
    |_____________________________|_____________________________|
    | MT-Level                    | MT-Safe, Fork 1-Safe        |
    |_____________________________|_____________________________|


FILES
  POSIX
     /usr/include/pthread.h /lib/libpthread.* /lib/libposix4.*

  Solaris
     /usr/include/thread.h /usr/include/sched.h /lib/libthread.*

SEE ALSO
     crle(1),           fork(2),            pthread_atfork(3THR),
     pthread_create(3THR), attributes(5), standards(5)

     Linker and Libraries Guide






SunOS 5.8           Last change: 22 Oct 1999                    7



