#include <stdio.h>
#include <omp.h>

#define NUM_THREADS 8
static long num_steps = 100000;
double step;

void main(void) {
    int i, nthreads;
    omp_set_num_threads(8);
    double pi, sum[NUM_THREADS];
    step = 1.0/(double)num_steps;
    omp_set_num_threads(NUM_THREADS);

    #pragma omp parallel
    {   int i, ID, nthrds;
        double x;

        for (i=id, sum[id]= 0.0; i < num_steps; i=i+nthrds) {
            x = (i + 0.5) * step;
            sum [id] += 4.0 / (1.0 + x * x);
        }
    }
    for(i=0, pi=0.0; i<nthreads; i++){
        pi += sum[i] * step;
    }
    printf("Pi is %ld\n",pi);
}

