/*
 * Sean Corbett
 * 03/14/17
 * CSCI480
 * Udacity HW3
*/

#include <limits.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include "utils.h"

/*
 * Midpoint represents segment of local histogram bin for thread to work on. If midpoint is
 * in local hist. bounds, find bin in subdivided local histogram and increment.
 */
__global__
void histogram_kernel(unsigned int* d_bins, const float* d_in, const int binCount, const float lMin, const float lMax, const int size) {  
    int midPoint = threadIdx.x + blockDim.x * blockIdx.x;
    if(midPoint >= size)
        return;
    float lRange = lMax - lMin;
    int bin = ((d_in[midPoint]-lMin) / lRange) * binCount;
    
    atomicAdd(&d_bins[bin], 1);
}

/*
 * Inclusive scan of kernel using increasing strides by i*=2.
 * Get surrounding lum vals and combine/add through inclusive
 * scan.
 */
__global__ 
void scan_kernel(unsigned int* d_bins, int size) {
    int midPoint = threadIdx.x + blockDim.x * blockIdx.x;
    if(midPoint >= size)
        return;
    
    for(int i = 1; i <= size; i *= 2) {
          int spot = midPoint - i; 
         
          unsigned int val = 0;
          if(spot >= 0)
              val = d_bins[spot];
          __syncthreads();
          if(spot >= 0)
              d_bins[midPoint] += val;
          __syncthreads();
    }
}

/*
 * Gets surrounding luminesence vals and places in shared mem array for
 * each pixel in picture.
 */
__global__
void reduce_minmax_kernel(const float* const d_in, float* d_out, const size_t size, int minMax) {
    extern __shared__ float shared[];
    
    int midPoint = threadIdx.x + blockDim.x * blockIdx.x;
    int id = threadIdx.x; 
    if(midPoint < size) {
        shared[id] = d_in[midPoint];
    } else {
        if(minMax == 0)
            shared[id] = FLT_MAX;
        else
            shared[id] = -FLT_MAX;
    }
    __syncthreads();
    
    if(midPoint >= size) {   
        if(id == 0) {
            if(minMax == 0) 
                d_out[blockIdx.x] = FLT_MAX;
            else
                d_out[blockIdx.x] = -FLT_MAX;

        }
        return;
    }
       
    for(unsigned int i = blockDim.x/2; i > 0; i /= 2) {
        if(id < i) {
            if(minMax == 0) {
                shared[id] = min(shared[id], shared[id+i]);
            } else {
                shared[id] = max(shared[id], shared[id+i]);
            }
        }
        
        __syncthreads();
    }
    
    if(id == 0) {
        d_out[blockIdx.x] = shared[0];
    }
}

/*
 * Get max size of threads to allocate to block based on max
 * number of bins remaining.
 */
int get_max_size(int n, int d) {
    return (int)ceil( (float)n/(float)d ) + 1;
}

/*
 * Reduce luminesence vals in bins until we reach desired lum threshold
 */
float reduce_minmax(const float* const d_in, const size_t size, int minMax) {
    int BLOCKSIZE = 32;
    size_t currentSize = size;
    float* d_currentIn;
    
    checkCudaErrors(cudaMalloc(&d_currentIn, sizeof(float) * size));    
    checkCudaErrors(cudaMemcpy(d_currentIn, d_in, sizeof(float) * size, cudaMemcpyDeviceToDevice));


    float* d_currentOut;
    
    dim3 thread_dim(BLOCKSIZE);
    const int shared_mem_size = sizeof(float)*BLOCKSIZE;
    
    while(1) {
        checkCudaErrors(cudaMalloc(&d_currentOut, sizeof(float) * get_max_size(currentSize, BLOCKSIZE)));
        
        dim3 block_dim(get_max_size(size, BLOCKSIZE));
        reduce_minmax_kernel<<<block_dim, thread_dim, shared_mem_size>>>(
            d_currentIn,
            d_currentOut,
            currentSize,
            minMax
        );
        cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaFree(d_currentIn));
        d_currentIn = d_currentOut;
        
        if(currentSize <  BLOCKSIZE) 
            break;
        
        currentSize = get_max_size(currentSize, BLOCKSIZE);
    }
    
    float h_out;
    cudaMemcpy(&h_out, d_currentOut, sizeof(float), cudaMemcpyDeviceToHost);
    cudaFree(d_currentOut);
    return h_out;
}

/*
 * Main function. Launches kernels, sets block sizes and dimensions, thread
 * sizes and dimensions, synchronizes and executes copy between CPU hist
 * GPU hist.
 */
void your_histogram_and_prefixsum(const float* const d_logLuminance,
                                  unsigned int* const d_cdf,
                                  float &min_logLum,
                                  float &max_logLum,
                                  const size_t numRows,
                                  const size_t numCols,
                                  const size_t numBins)
{
    const size_t size = numRows*numCols;
    min_logLum = reduce_minmax(d_logLuminance, size, 0);
    max_logLum = reduce_minmax(d_logLuminance, size, 1);
    
    unsigned int* d_bins;
    size_t histo_size = sizeof(unsigned int)*numBins;

    checkCudaErrors(cudaMalloc(&d_bins, histo_size));    
    checkCudaErrors(cudaMemset(d_bins, 0, histo_size));  
    dim3 thread_dim(1024);
    dim3 hist_block_dim(get_max_size(size, thread_dim.x));
    histogram_kernel<<<hist_block_dim, thread_dim>>>(d_bins, d_logLuminance, numBins, min_logLum, max_logLum, size);
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
    
    unsigned int h_out[100];
    cudaMemcpy(&h_out, d_bins, sizeof(unsigned int)*100, cudaMemcpyDeviceToHost);
    
    dim3 scan_block_dim(get_max_size(numBins, thread_dim.x));

    scan_kernel<<<scan_block_dim, thread_dim>>>(d_bins, numBins);
    
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
    cudaMemcpy(&h_out, d_bins, sizeof(unsigned int)*100, cudaMemcpyDeviceToHost);
    cudaMemcpy(d_cdf, d_bins, histo_size, cudaMemcpyDeviceToDevice);
    checkCudaErrors(cudaFree(d_bins));
}
