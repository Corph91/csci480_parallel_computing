#include <stdio.h>
#include <omp.h>

int main(){

    double avg = 0.0;
    int array[10];
    int j;
    int size = sizeof(array) / sizeof(array[0]);

    for(int i = 0; i < 10; i++){
        array[i] = i+1;
    }

    #pragma omp parallel for reduction (+:avg)
        for(j = 0; j<size; j++){
            avg += array[j];
        }
        avg = avg/size;
    
    printf("%lf",avg);

    return 0;
}