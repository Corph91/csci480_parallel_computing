#include <stdio.h>
#include <omp.h>

int main(){
    printf("procs %d\n",omp_get_num_procs());
    printf("threads %d\n",omp_get_num_threads());
    printf("num %d\n", omp_get_thread_num());

    #pragma omp parallel
    {
        printf("procs %d\n",omp_get_num_procs());
        printf("threads %d\n",omp_get_num_threads());
        printf("num %d\n", omp_get_thread_num());
    }

    return 0;
}