#include <stdio.h>
#include <omp.h>

int main(){
    int a[10];
    int b[10];
    int x[10];
    int c[10];
    int size = 10;

    #pragma omp parallel for
    for(int i=0; i <size; i++){
        a[i] = i+1;
        b[i] = i+1;
        c[i] = i+1;
        x[i] = i+1;
    }

    for(int i=0; i<size;i++){
        printf("Array A before: %d\n",a[i]);
    }
    for(int i=0; i<size;i++){
        printf("Array X before: %d\n",x[i]);
    }

    #pragma omp parallel for
    for(int i=1; i<size; i++){
        x[i] = a[i] + b[i + 1];
        a[i] = 2 * x[i-1] + c[i + 1];
    }

    for(int i=0; i<size;i++){
        printf("Array A after: %d\n",a[i]);
    }
    for(int i=0; i<size;i++){
        printf("Array X after: %d\n",x[i]);
    }



    return 0;
}