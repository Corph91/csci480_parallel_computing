#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <string.h>
#include <time.h>

void count_sort(int a[], int n) {
    double start_time = omp_get_wtime();
    int count;
    int* temp = malloc(n*sizeof(int));

    #pragma omp parallel for private(count)
    for (int i = 0; i < n; i++) { 
        count = 0;
        for (int j = 0; j < n; j++) { 
            if (a[j] < a[i])
            count++;
            else if (a[j] == a[i] && j < i)
            count++; 
        }
        temp[count] = a[i];
    } 

    #pragma omp parallel for
    for(int i=0; i<n; i++){
        a[i] = temp[i];
    }
    free(temp);

    double end_time = omp_get_wtime();
    printf("Parallel run time %lf\n\n",end_time - start_time);

}

void count_sort_no_paralle(int a[], int n){
    double start_time = omp_get_wtime();
    int count;
    int* temp = malloc(n*sizeof(int));

    for (int i = 0; i < n; i++) { 
        count = 0;
        for (int j = 0; j < n; j++) { 
            if (a[j] < a[i])
            count++;
            else if (a[j] == a[i] && j < i)
            count++; 
        }
        temp[count] = a[i];
    } 

    memcpy(a, temp, n*sizeof(int));
    free(temp);

    double end_time = omp_get_wtime();
    printf("Non-parallel run time %lf\n\n",end_time - start_time);
}

int cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

void qsort_test(int a[], int n){

    double start_time = omp_get_wtime();

    qsort(a, n, sizeof(int), cmpfunc);
    
    double end_time = omp_get_wtime();
    printf("Qsort run time %lf\n",end_time - start_time);
}

void main(){
    int size = 100000;
    int arry[size];
    srand((int) time(0));

    #pragma omp parallel for
    for(int i = 0; i<size; i++){
        arry[i] = ((rand()%1000000) +1);
    }

    count_sort(arry,size);
    count_sort_no_paralle(arry,size);
    qsort_test(arry,size);
}