Sean Corbett
04/21/2017
CSCI480
CUDA Homework 5

For this assignment I chose to use as many straight-forward kernel calls as possible, while taking advantage of
more advanced features such as the "int2" struct and other CUDA goodies. The program begins by mapping the features
of the source image to an array. After this kernel completes, we then perform a Von Neuman stencil on the the source
image to create mask with which to overlay on the target image given coalesced source image brightness/pixel/feature
information. With this mask, we then seperate both images into their respective red, blue, and yellow channels via RGB
kernel (a scatter operation), from which we then convert the uchar array for each image's channels arrays to floats
via a conversion kernel. The core operation of the program is then executed via a probabilistic "guessing" of the
internal value (inside the mask) of the pixels inside the border region of the mask. We continue guessing until
an ideal pixel value is found, and then placed inside the output array. This operation involves use of a 1D
and 2D Von Neumann stencils to gather information from a given pixels neighboring pixels. Upon filling in
the internal pixels over 800 iterations, recombine our seperate RGB channels via gather operation in the
recombineChannels kernel and output the cloned image.
