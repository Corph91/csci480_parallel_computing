/*
 * Sean Corbett
 * 04/21/2017
 * CSCI480
 * CUDA HW 5
 */

#define JACOBI_ITR 800
#define INTERIOR_PX_VAL 5
#include "utils.h"
#include <thrust/host_vector.h>

/*
 * Here we guess where to place and integrate our image.
 */
__global__ void poissonJacobi(float* const ImageGuess_next, const float* const ImageGuess_prev, const unsigned char* const source, const unsigned char* const target, const unsigned char* const d_sourceMaskInteriorMap, const unsigned char* const d_sourceMask, const size_t numRowsSource, const size_t numColsSource){

  const int2 t_id2 = make_int2(blockIdx.x * blockDim.x + threadIdx.x,blockIdx.y * blockDim.y + threadIdx.y);

  if (t_id2.x >= numColsSource || t_id2.y >= numRowsSource){
      return;
  }

  const int t_id1 = t_id2.y * numColsSource + t_id2.x;

  if (d_sourceMaskInteriorMap[t_id1] != INTERIOR_PX_VAL){
      return;
  }

  const unsigned char spx = source[t_id1];
  char vn_stencil2[4][2] = {{0,1},{1,0},{0,-1},{-1,0}};
  float sum1 = 0.0f;
  float sum2 = 0.0f;

  // I decided not to check for boundaries as (though perhaps an unsafe
  // assumption) the image is likely not going to be placed at an edge.
  for (char i = 0; i < 4; i++) {
    int vn_stencil1 = (t_id2.x + vn_stencil2[i][0]) + (t_id2.y + vn_stencil2[i][1]) * numColsSource;
    if (d_sourceMaskInteriorMap[vn_stencil1] == INTERIOR_PX_VAL){
        sum1 += ImageGuess_prev[vn_stencil1];
    }
    else if (d_sourceMask[vn_stencil1] == 1){
        sum1 += (float)target[vn_stencil1];
    }

    sum2 += (float)(spx - source[vn_stencil1]);
  }

  float avg = (sum1 + sum2) / 4.0f;
  ImageGuess_next[t_id1] = fmin(255.0f, fmax(0.0f, avg));
}

// First map brightness of input image and create a mask
__global__ void mapToSource(const uchar4* const d_sourceImg, unsigned char* const d_sourceMask, const size_t numRowsSource, const size_t numColsSource){
  const int2 t_id2 = make_int2(blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y);
  if (t_id2.x >= numColsSource || t_id2.y >= numRowsSource){
      return;
  }

  const int t_id1 = t_id2.y * numColsSource + t_id2.x;

  const uchar4 px = d_sourceImg[t_id1];
  int brightness = px.x + px.y + px.z;
  if (brightness < 765){
      d_sourceMask[t_id1] = 1;
  }
}

// Von Neumann stencil's aren't anything new by this point,
// but we use one to coalesce image brightness/pixels/features.
__global__ void vonNeumann2D(const unsigned char* const d_in, unsigned char* const d_out,
                       const size_t numRows, const size_t numCols)
{
  const int2 t_id2 = make_int2(blockIdx.x * blockDim.x + threadIdx.x,
                                      blockIdx.y * blockDim.y + threadIdx.y);
  if (t_id2.x >= numCols || t_id2.y >= numRows){
    return;
  }

  const int t_id1 = t_id2.y * numCols + t_id2.x;

  if (t_id2.y){
    d_out[t_id1] += d_in[(t_id2.y-1) * numCols + t_id2.x];
  }
  if (t_id2.x < (numCols-1)){
    d_out[t_id1] += d_in[t_id2.y * numCols + (t_id2.x+1)];
  }
  if (t_id2.y < (numRows-1)){
    d_out[t_id1] += d_in[(t_id2.y+1) * numCols + t_id2.x];
  }
  if (t_id2.x){
    d_out[t_id1] += d_in[t_id2.y * numCols + (t_id2.x-1)];
  }

}

// We need to seperate our RGB, and preferably do it one kernel call.
__global__ void separateChannels(const uchar4* const inputImageRGBA,const size_t numRows, const size_t numCols, unsigned char* const redChannel, unsigned char* const greenChannel, unsigned char* const blueChannel){
  const int2 t_2D_pos = make_int2( blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y);
  if ( t_2D_pos.y >= numRows || t_2D_pos.x >= numCols ){
    return;
  }

  const int t_1D_pos = t_2D_pos.y * numCols + t_2D_pos.x;

  const uchar4 px = inputImageRGBA[t_1D_pos];
  redChannel[t_1D_pos] = px.x;
  greenChannel[t_1D_pos] = px.y;
  blueChannel[t_1D_pos] = px.z;
}

// Recombine individual channels via map operation based on t_id2.x and .y id
__global__ void recombineChannels(const float* const redChannel, const float* const greenChannel, const float* const blueChannel, uchar4* const outputImageRGBA, const size_t numRows, const size_t numCols, const unsigned char* const d_sourceMaskInteriorMap){
  const int2 t_id2 = make_int2(blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y);
  if (t_id2.x >= numCols || t_id2.y >= numRows){
    return;
  }

  const int t_id1 = t_id2.y * numCols + t_id2.x;
  if (d_sourceMaskInteriorMap[t_id1] != INTERIOR_PX_VAL){
    return;
  }


  unsigned char red   = redChannel[t_id1];
  unsigned char green = greenChannel[t_id1];
  unsigned char blue  = blueChannel[t_id1];

  uchar4 outputPixel = make_uchar4(red, green, blue, 255);

  outputImageRGBA[t_id1] = outputPixel;
}

//  Map operation to convert uchar to float
__global__ void copyInteriorToFloat(float* const large, const unsigned char* const small,  const size_t numRows,  const size_t numCols,  const unsigned char* const d_sourceMaskInteriorMap){
  const int2 t_id2 = make_int2(blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y);

  if (t_id2.x >= numCols || t_id2.y >= numRows){
    return;
  }


  const int t_id1 = t_id2.y * numCols + t_id2.x;


  if (d_sourceMaskInteriorMap[t_id1] == INTERIOR_PX_VAL){
    large[t_id1] = (float)small[t_id1];
  }

  else{
    large[t_id1] = 0.f;
  }

}



void your_blend(const uchar4* const h_sourceImg,  const size_t numRowsSource, const size_t numColsSource, const uchar4* const h_destImg, uchar4* const h_blendedImg){

  uchar4* d_sourceImg;
  unsigned char* d_sourceMask;

  unsigned char* d_sourceMaskInteriorMap;

  uchar4* d_targetImg;
  unsigned char* d_targetRed;
  unsigned char* d_targetGreen;
  unsigned char* d_targetBlue;

  unsigned char* d_sourceRed;
  unsigned char* d_sourceGreen;
  unsigned char* d_sourceBlue;

  float* d_prevRed;
  float* d_prevGreen;
  float* d_prevBlue;
  float* d_nextRed;
  float* d_nextGreen;
  float* d_nextBlue;
  const dim3 numThreads(16,32,1);
  const dim3 numBlocks(1 + numColsSource / numThreads.x, 1 + numRowsSource / numThreads.y, 1);
  const int imageSize = sizeof(uchar4)*numColsSource*numRowsSource;
  const int channelSize = sizeof(unsigned char)*numColsSource*numRowsSource;


  checkCudaErrors(cudaMalloc(&d_sourceImg, imageSize));
  checkCudaErrors(cudaMemcpy(d_sourceImg, h_sourceImg, imageSize, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMalloc(&d_sourceMask, channelSize));
  checkCudaErrors(cudaMemset(d_sourceMask, 0, channelSize));


  // Map source image, get brightness vals from source image
  mapToSource<<<numBlocks, numThreads>>>(d_sourceImg, d_sourceMask, numRowsSource, numColsSource);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  checkCudaErrors(cudaMalloc(&d_sourceMaskInteriorMap, channelSize));
  checkCudaErrors(cudaMemcpy(d_sourceMaskInteriorMap, d_sourceMask, channelSize, cudaMemcpyDeviceToDevice));

  // Stencil image and produce mask of characteristics from source image
  vonNeumann2D<<<numBlocks, numThreads>>>(d_sourceMask, d_sourceMaskInteriorMap, numRowsSource, numColsSource);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  checkCudaErrors(cudaMalloc(&d_sourceRed, channelSize));
  checkCudaErrors(cudaMalloc(&d_sourceGreen, channelSize));
  checkCudaErrors(cudaMalloc(&d_sourceBlue, channelSize));

  //Separate into RGB channels in single kernel (for efficiency) on image we are sourcing
  separateChannels<<<numBlocks, numThreads>>>(d_sourceImg, numRowsSource, numColsSource,d_sourceRed, d_sourceGreen, d_sourceBlue);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  checkCudaErrors(cudaMalloc(&d_targetImg, imageSize));
  checkCudaErrors(cudaMemcpy(d_targetImg, h_destImg, imageSize, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMalloc(&d_targetRed, channelSize));
  checkCudaErrors(cudaMalloc(&d_targetGreen, channelSize));
  checkCudaErrors(cudaMalloc(&d_targetBlue, channelSize));

  // Call one more time for second image (target)
  separateChannels<<<numBlocks, numThreads>>>(d_targetImg, numRowsSource, numColsSource,d_targetRed, d_targetGreen, d_targetBlue);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());


  const int size = sizeof(float)*numColsSource*numRowsSource;
  // Even more error checking.
  checkCudaErrors(cudaMalloc(&d_prevRed, size));
  checkCudaErrors(cudaMalloc(&d_prevGreen, size));
  checkCudaErrors(cudaMalloc(&d_prevBlue, size));
  checkCudaErrors(cudaMalloc(&d_nextRed, size));
  checkCudaErrors(cudaMalloc(&d_nextGreen, size));
  checkCudaErrors(cudaMalloc(&d_nextBlue, size));

  // Convert uchar per channel to float have to call for each channel on both images
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_prevRed, d_sourceRed, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_prevGreen, d_sourceGreen, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_prevBlue, d_sourceBlue, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_nextRed, d_sourceRed, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_nextGreen, d_sourceGreen, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  copyInteriorToFloat<<<numBlocks, numThreads>>>(d_nextBlue, d_sourceBlue, numRowsSource, numColsSource, d_sourceMaskInteriorMap);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());


  // 800 iterations of poissonJacobi checking next or prev positions in imageSize
  // based on i%2. I tried hard to find a way around 800 iterations, but there
  // really is none. End the end, follow the directions.
  for (int i = 0; i < JACOBI_ITR; i++) {
    if (i % 2 == 0) {
      poissonJacobi<<<numBlocks, numThreads>>>(d_nextRed, d_prevRed,d_sourceRed, d_targetRed,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
      poissonJacobi<<<numBlocks, numThreads>>>(d_nextGreen, d_prevGreen,d_sourceGreen, d_targetGreen,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
      poissonJacobi<<<numBlocks, numThreads>>>(d_nextBlue, d_prevBlue,d_sourceBlue, d_targetBlue,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
    } else {
      poissonJacobi<<<numBlocks, numThreads>>>(d_prevRed, d_nextRed,d_sourceRed, d_targetRed,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
      poissonJacobi<<<numBlocks, numThreads>>>(d_prevGreen, d_nextGreen,d_sourceGreen, d_targetGreen,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
      poissonJacobi<<<numBlocks, numThreads>>>(d_prevBlue, d_nextBlue,d_sourceBlue, d_targetBlue,d_sourceMaskInteriorMap, d_sourceMask,numRowsSource, numColsSource);
    }
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
  }

  // Recombine RGB and we're outta here
  recombineChannels<<<numBlocks, numThreads>>>(d_prevRed,d_prevGreen,d_prevBlue,d_targetImg,numRowsSource,numColsSource,d_sourceMaskInteriorMap);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  // Check for everything that could cause an error
  checkCudaErrors(cudaMemcpy(h_blendedImg, d_targetImg, imageSize, cudaMemcpyDeviceToHost));
  checkCudaErrors(cudaFree(d_sourceImg));
  checkCudaErrors(cudaFree(d_sourceMask));
  checkCudaErrors(cudaFree(d_sourceMaskInteriorMap));
  checkCudaErrors(cudaFree(d_targetImg));
  checkCudaErrors(cudaFree(d_targetRed));
  checkCudaErrors(cudaFree(d_targetGreen));
  checkCudaErrors(cudaFree(d_targetBlue));
  checkCudaErrors(cudaFree(d_sourceRed));
  checkCudaErrors(cudaFree(d_sourceGreen));
  checkCudaErrors(cudaFree(d_sourceBlue));
  checkCudaErrors(cudaFree(d_prevRed));
  checkCudaErrors(cudaFree(d_prevGreen));
  checkCudaErrors(cudaFree(d_prevBlue));
  checkCudaErrors(cudaFree(d_nextRed));
  checkCudaErrors(cudaFree(d_nextGreen));
  checkCudaErrors(cudaFree(d_nextBlue));
}
